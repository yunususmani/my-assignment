package com.takeaway.restaurant.admin.apis.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import com.takeaway.restaurant.admin.apis.controller.RestaurantController;
import com.takeaway.restaurant.admin.apis.entity.Restaurant;
import com.takeaway.restaurant.admin.apis.model.CreateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.model.RestaurantResponse;
import com.takeaway.restaurant.admin.apis.model.UpdateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.repository.RestaurantRepository;
import com.takeaway.restaurant.admin.apis.service.RestaurantService;

@ActiveProfiles("junittests")
@SpringBootTest
public class ControllerTest {

	@Autowired
	private RestaurantController controller;
	
	@Autowired
	private RestaurantService service;
		
	@Autowired
	private RestaurantRepository repository;
	
	@Test
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}
    
	@Test
	/**
	 * createRestaurantTest
	 */
	public void test1() {
		service.setShouldPublish(false);
		controller.setRestaurantService(service);
		String restaurantName = "delicios food service";
		String status = "OPEN";
		
		CreateRestaurantRequest request = CreateRestaurantRequest.builder()
															     .restaurantName(restaurantName)
															     .status(status)
															     .build();
		
		ResponseEntity<?> response = controller.createRestaurant(request);
		
		assertTrue(response.getStatusCode() == HttpStatus.CREATED);
	}
	/**
	 * validateCreatedRestaurant
	 */
	@Test
	public void test2() {
		service.setShouldPublish(false);
		controller.setRestaurantService(service);
		Integer id = 1;
		String restaurantName = "delicios food service";
		String status = "OPEN";
		
		Optional<Restaurant> findById = repository.findById(1);
		
		assertTrue(findById.isPresent());
		assertEquals(id, findById.get().getId());
		assertEquals(restaurantName.toUpperCase(), findById.get().getName());
		assertEquals(status, findById.get().getStatus().toString());
	}

	/**
	 * testUpdateRestaurant
	 */
	@Test
	public void test3() {
		service.setShouldPublish(false);
		controller.setRestaurantService(service);
		Integer id = 1;
		String restaurantName = "delicios food services";
		String status = "CLOSED";
		
		UpdateRestaurantRequest request = UpdateRestaurantRequest.builder()
															 .id(id)
															 .restaurantName(restaurantName)
															 .status(status)
															 .build();
		
		ResponseEntity<?> apiResponse = controller.updateRestaurant(request);
		assertTrue(apiResponse.getStatusCode() == HttpStatus.NO_CONTENT);
		
		Optional<Restaurant> findById = repository.findById(id);
		
		assertTrue(findById.isPresent());
		assertEquals(restaurantName.toUpperCase(), findById.get().getName());
		assertEquals(status, findById.get().getStatus().toString());
	}
	
	/**
	 * testGetRestaurant
	 */
	@Test
	public void test4() {
		service.setShouldPublish(false);
		controller.setRestaurantService(service);
		boolean invalidTestaurantId = false;
		try {
			ResponseEntity<?> apiResponse = controller.getRestaurant(-1);
		}
		catch(Exception e) {
			invalidTestaurantId = true;
		}
		
		assertTrue(invalidTestaurantId);
		
		String restaurantName = "delicios food services";
		String status = "CLOSED";
		
		ResponseEntity<?> apiResponse = controller.getRestaurant(1);
		assertTrue(apiResponse.getStatusCode() == HttpStatus.OK);

		RestaurantResponse response = (RestaurantResponse) apiResponse.getBody();
		assertTrue(response != null);
		assertEquals(restaurantName.toUpperCase(), response.getRestaurantName());
		assertEquals(status, response.getStatus());
		
	}
	/**
	 * testDeleteRestaurant
	 */
	@Test
	public void test5() {
		service.setShouldPublish(false);
		controller.setRestaurantService(service);
		boolean invalidTestaurantId = false;
		try {
			ResponseEntity<?> apiResponse = controller.getRestaurant(-1);
		}
		catch(Exception e) {
			invalidTestaurantId = true;
		}
		
		assertTrue(invalidTestaurantId);
		
		Integer id = 1;
		controller.deleteRestaurant(id);
		
		Optional<Restaurant> findById = repository.findById(id);
		assertTrue(!findById.isPresent());
	}
	
}
