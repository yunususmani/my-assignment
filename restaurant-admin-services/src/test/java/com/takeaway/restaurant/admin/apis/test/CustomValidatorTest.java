package com.takeaway.restaurant.admin.apis.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import javax.validation.ConstraintValidatorContext;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.takeaway.restaurant.admin.apis.validation.StringValidator;
import com.takeaway.restaurant.admin.apis.validation.ValidateString;

@ExtendWith(MockitoExtension.class)

class CustomValidatorTest {

	@Mock
	private ValidateString validateString;
		
	@Mock
	private ConstraintValidatorContext constraintValidatorContext;
	
	@Test
	void validateStatus() {
		
		when(validateString.acceptedValues()).thenReturn(new String[] {"OPEN", "CLOSED"});
		
		
		StringValidator validator = new StringValidator();
		validator.initialize(validateString);
		
		boolean valid = validator.isValid("OPEN", constraintValidatorContext);
		assertTrue(valid);
		
		//case insensitive customer type match
		boolean valid2 = validator.isValid("Closed", constraintValidatorContext);
		assertTrue(valid2);
		
		// invalid customer type
		boolean valid3 = validator.isValid("Invalid", constraintValidatorContext);
		assertFalse(valid3);
	}

}
