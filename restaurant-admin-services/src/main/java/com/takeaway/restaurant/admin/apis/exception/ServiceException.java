package com.takeaway.restaurant.admin.apis.exception;

import com.takeaway.restaurant.admin.apis.model.ErrorResponse;

public class ServiceException extends RuntimeException{

	private ErrorResponse errorResponse;
	
	public ServiceException(ErrorResponse error) {
        super();
        this.errorResponse = error;
    }
	
	public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

	public ErrorResponse getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(ErrorResponse errorResponse) {
		this.errorResponse = errorResponse;
	}
}
