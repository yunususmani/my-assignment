package com.takeaway.restaurant.admin.apis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantResponse {
	
	private Integer id;
	
	private String restaurantName;
	
	private String status;
}
