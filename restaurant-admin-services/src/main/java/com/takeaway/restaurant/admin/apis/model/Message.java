package com.takeaway.restaurant.admin.apis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Message {

	private String messageId;
	private Integer id;
	private String restaurantName;
	private String status;
	private String eventType;
}
