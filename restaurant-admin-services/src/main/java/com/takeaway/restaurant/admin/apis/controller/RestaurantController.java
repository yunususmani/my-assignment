package com.takeaway.restaurant.admin.apis.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.takeaway.restaurant.admin.apis.model.CreateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.model.RestaurantResponse;
import com.takeaway.restaurant.admin.apis.model.UpdateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.service.IRestaurantService;

/**
 * Restaurant management CRUD operations for ADMINs
 * @author Mohdyunus.Usmani
 *
 */
@RestController
@RequestMapping(path="/api/admin/restaurant")
public class RestaurantController {
	
	@Autowired
	private IRestaurantService restaurantService;
	
	/** 
	 * returns all available restaurants in the system.
	 * @return
	 */
	@GetMapping
	public ResponseEntity<?> getAllRestaurants(){
		
		List<RestaurantResponse> restaurants = restaurantService.getAllRestaurants();
		
		return new ResponseEntity<>(restaurants, HttpStatus.OK);
	}
	
	/**
	 * creates restaurant 
	 * @param request
	 * @return
	 */
	@PostMapping
	public ResponseEntity<?> createRestaurant(@Valid @RequestBody CreateRestaurantRequest request){
			
		restaurantService.createRestaurant(request);
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	/**
	 * update restaurant
	 * @param request
	 * @return
	 */
	@PutMapping
	public ResponseEntity<?> updateRestaurant(@Valid @RequestBody UpdateRestaurantRequest request){
		
		restaurantService.updateRestaurant(request);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * delete restaurant by id
	 * @param id
	 * @return
	 */
	@DeleteMapping(path="/{id}")
	public ResponseEntity<?> deleteRestaurant(@PathVariable Integer id){
		
		restaurantService.deleteRestaurant(id);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * get restaurant by id
	 * @param id
	 * @return
	 */
	@GetMapping(path="/{id}")
	public ResponseEntity<?> getRestaurant(@PathVariable Integer id){
		
		RestaurantResponse response = restaurantService.getRestaurantById(id);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public IRestaurantService getRestaurantService() {
		return restaurantService;
	}

	public void setRestaurantService(IRestaurantService restaurantService) {
		this.restaurantService = restaurantService;
	}

	
}
