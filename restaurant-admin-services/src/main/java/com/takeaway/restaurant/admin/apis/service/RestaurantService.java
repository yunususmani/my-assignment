package com.takeaway.restaurant.admin.apis.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.takeaway.restaurant.admin.apis.entity.Restaurant;
import com.takeaway.restaurant.admin.apis.exception.ServiceException;
import com.takeaway.restaurant.admin.apis.model.CreateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.model.ErrorResponse;
import com.takeaway.restaurant.admin.apis.model.Message;
import com.takeaway.restaurant.admin.apis.model.RestaurantResponse;
import com.takeaway.restaurant.admin.apis.model.Status;
import com.takeaway.restaurant.admin.apis.model.UpdateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.repository.RestaurantRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Service layer for restaurant APIs
 * @author Mohdyunus.Usmani
 *
 */
@Slf4j
@Service
public class RestaurantService implements IRestaurantService{

	@Autowired
	private RestaurantRepository repository;

	@Autowired
	private MessagePublisher messagePublisher;
	
	/**
	 * creates restaurant in the system
	 */
	@Override
	public int createRestaurant(CreateRestaurantRequest payload) {
		
		log.info("create restaurant request received: {}", payload);
		Restaurant restaurant = Restaurant.builder().name(payload.getRestaurantName().toUpperCase())
							.status(Status.valueOf(payload.getStatus().toUpperCase()))
							.build();
		
		
		Restaurant persisted = repository.save(restaurant);
		
		log.info("restaurant details are persisted into database");
		
		sendEventToRabbitMQ(persisted, "CREATED");
		log.info("message sent to rebit mq");
		
		return persisted.getId();
	}

	/**
	 * updates restaurant details
	 */
	@Override
	public int updateRestaurant(UpdateRestaurantRequest payload) {
		log.info("update restaurant request received: {}", payload);
		Restaurant restaurant = Restaurant.builder()
										  .id(payload.getId())
										  .name(payload.getRestaurantName().toUpperCase())
										  .status(Status.valueOf(payload.getStatus().toUpperCase()))
										  .build();

		Restaurant persisted = repository.save(restaurant);
		
		log.info("restaurant details are updated into database");
		
		sendEventToRabbitMQ(persisted, "UPDATED");
		log.info("message sent to rebit mq");
		
		return persisted.getId();
		
	}

	/**
	 * delete restaurant by id
	 */
	@Override
	public int deleteRestaurant(Integer id) {
		
		log.info("delete restaurant request received: {}", id);
		if(id <= 0 || !repository.findById(id).isPresent()) {
			log.error("request validation failed");
			List<String> errors = new ArrayList<>();
			errors.add("restaurantId is missing or invalid");
			ErrorResponse errorResponse = ErrorResponse.builder()
					.message("Request Validation Failed.")
					.errors(errors)
					.build();
			
			throw new ServiceException(errorResponse);		
		}
		
		repository.deleteById(id);
		log.info("restaurant is deleted from database");
		
		sendEventToRabbitMQ(Restaurant.builder().id(id).build(), "DELETED");
		log.info("message sent to rebit mq");
		
		return id;
	}

	/**
	 * Get restaurant by id
	 */
	@Override
	public RestaurantResponse getRestaurantById(Integer id) {
		log.info("get restaurant request received: {}", id);
		
		if(id <= 0 || !repository.findById(id).isPresent()) {
			log.error("request validation failed");
			List<String> errors = new ArrayList<>();
			errors.add("restaurantId is missing or invalid");
			ErrorResponse errorResponse = ErrorResponse.builder()
					.message("Request Validation Failed.")
					.errors(errors)
					.build();
			
			throw new ServiceException(errorResponse);		
		}
		
		Restaurant restaurant = repository.getById(id);

		log.info("restaurant details retrieved from databse:");
		RestaurantResponse response = RestaurantResponse.builder()
						  .id(restaurant.getId())
						  .restaurantName(restaurant.getName())
						  .status(restaurant.getStatus().toString())
						  .build();
		return response;
	}

	/**
	 * returns all restaurants from the system.
	 */
	@Override
	public List<RestaurantResponse> getAllRestaurants() {
		
		log.info("get all restaurants request received");
		List<Restaurant> restaurants = repository.findAll();
		log.info("restaurants details retrieved from database");
		
		List<RestaurantResponse> response = new ArrayList<>();
		restaurants.forEach(restaurant ->{
			RestaurantResponse rest = RestaurantResponse.builder()
							  .id(restaurant.getId())
							  .restaurantName(restaurant.getName())
							  .status(restaurant.getStatus().toString())
							  .build();
			response.add(rest);
		});
		
		return response;
	}
	
	/**
	 * sends message event to rabbit mq
	 * @param restaurant
	 * @param eventType
	 */
	public void sendEventToRabbitMQ(Restaurant restaurant, String eventType) {
		
		String messageId = UUID.randomUUID().toString();
		
		Message message = Message.builder()
			   .eventType(eventType)
			   .id(restaurant.getId())
			   .messageId(messageId)
			   .restaurantName(restaurant.getName())
			   .status(restaurant.getStatus() == null ? null : restaurant.getStatus().toString())
			   .build();
		
		if(shouldPublish)
			messagePublisher.publish(message);
	}

	public MessagePublisher getMessagePublisher() {
		return messagePublisher;
	}
	
	private Boolean shouldPublish = true;

	public Boolean getShouldPublish() {
		return shouldPublish;
	}

	public void setShouldPublish(Boolean shouldPublish) {
		this.shouldPublish = shouldPublish;
	}
	
	
}
