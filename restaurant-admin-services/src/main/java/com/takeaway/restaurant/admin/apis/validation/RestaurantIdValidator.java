package com.takeaway.restaurant.admin.apis.validation;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.takeaway.restaurant.admin.apis.entity.Restaurant;
import com.takeaway.restaurant.admin.apis.repository.RestaurantRepository;

/**
 * Custom validator implemnted for resturant ID
 * @author Mohdyunus.Usmani
 *
 */
public class RestaurantIdValidator implements ConstraintValidator<ValidateRestaurantId, Integer>{

	@Autowired
	private RestaurantRepository restaurantRepository;
	
	
    @Override
    public void initialize(ValidateRestaurantId constraintAnnotation) {
    	//nothing to initialize
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
    	
    	if(value == null)
    		return true;
    	
    	Optional<Restaurant> findById = restaurantRepository.findById(value);
    	
    	return findById.isPresent();
    }

}