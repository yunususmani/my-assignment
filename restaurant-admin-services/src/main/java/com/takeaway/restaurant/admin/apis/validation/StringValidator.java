package com.takeaway.restaurant.admin.apis.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * custom validator implemnted to allow only open and closed value as status in request
 * @author Mohdyunus.Usmani
 *
 */
public class StringValidator implements ConstraintValidator<ValidateString, String>{

    private List<String> valueList;
    
    @Override
    public void initialize(ValidateString constraintAnnotation) {
        valueList = new ArrayList<String>();

        for(String val : constraintAnnotation.acceptedValues()) {
            valueList.add(val.toUpperCase());
        }
        
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
    	
    	if(value == null)
    		return false;
    	
        return valueList.contains(value.toUpperCase());
    }

}