package com.takeaway.restaurant.admin.apis.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.takeaway.restaurant.admin.apis.entity.Restaurant;

@Transactional
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer>{

}
