package com.takeaway.restaurant.admin.apis.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.takeaway.restaurant.admin.apis.config.MQConfig;
import com.takeaway.restaurant.admin.apis.model.Message;

@Component
public class MessagePublisher {

	@Autowired
	private RabbitTemplate template;
	
	public void publish(Message message) {
		template.convertAndSend(MQConfig.EXCHANGE, MQConfig.ROUTING_KEY, message);
	}
}
