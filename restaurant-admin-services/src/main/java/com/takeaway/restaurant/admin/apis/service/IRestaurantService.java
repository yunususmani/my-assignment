package com.takeaway.restaurant.admin.apis.service;

import java.util.List;

import com.takeaway.restaurant.admin.apis.entity.Restaurant;
import com.takeaway.restaurant.admin.apis.model.CreateRestaurantRequest;
import com.takeaway.restaurant.admin.apis.model.RestaurantResponse;
import com.takeaway.restaurant.admin.apis.model.UpdateRestaurantRequest;

public interface IRestaurantService {

	public int createRestaurant(CreateRestaurantRequest payload);
	
	public int updateRestaurant(UpdateRestaurantRequest payload);
	
	public int deleteRestaurant(Integer id);
	
	public RestaurantResponse getRestaurantById(Integer id);
	
	public List<RestaurantResponse> getAllRestaurants();
}
