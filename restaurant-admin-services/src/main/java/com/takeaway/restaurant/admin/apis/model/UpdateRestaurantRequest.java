package com.takeaway.restaurant.admin.apis.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.takeaway.restaurant.admin.apis.validation.ValidateRestaurantId;
import com.takeaway.restaurant.admin.apis.validation.ValidateString;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UpdateRestaurantRequest {

	@NotNull(message="id is missing or invalid")
	@Min(value=1, message="id is missing or invalid")
	@ValidateRestaurantId(message="id is missing or invalid")
	private Integer id; 
	
	@NotNull(message="restaurantName is missing or invalid")
	@Size(max=100, min=3, message="restaurantName is missing or invalid")
	private String restaurantName;
	
	@ValidateString(acceptedValues={"OPEN", "CLOSED"}, message="status is missing or invalid")
	private String status;
}
