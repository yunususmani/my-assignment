package com.takeaway.restaurant.admin.apis.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = StringValidator.class)
@Target({ TYPE, FIELD, ElementType.PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Documented
public @interface ValidateString {

	String message() default "{com.takeaway.restaurant.serviceeventproducer.model.message}";
	
    String[] acceptedValues();

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { }; 
}