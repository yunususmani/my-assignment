package com.takeaway.restaurant.admin.apis.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = RestaurantIdValidator.class)
@Target({ TYPE, FIELD, PARAMETER, ElementType.METHOD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Documented
public @interface ValidateRestaurantId {
	
	String message() default "{com.takeaway.restaurant.admin.apis.validation.model.RestaurantRequest.message}";
	
    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { }; 
}