package com.takeaway.restaurant.admin.apis.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Data
@Builder
public class ErrorResponse {

	private List<String> errors;
	
	private String message;
	
}
