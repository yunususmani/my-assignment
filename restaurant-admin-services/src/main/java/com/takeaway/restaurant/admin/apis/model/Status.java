package com.takeaway.restaurant.admin.apis.model;

public enum Status {

	OPEN, CLOSED
}
