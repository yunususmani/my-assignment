package com.takeaway.restaurant.admin.apis.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.takeaway.restaurant.admin.apis.model.ErrorResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
	  MethodArgumentNotValidException ex, 
	  HttpHeaders headers, 
	  HttpStatus status, 
	  WebRequest request) {
		log.error("Request paramaters validation failed {}", ex.getMessage());
	    List<String> errors = new ArrayList<String>();
	    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
	        errors.add(error.getDefaultMessage());
	    }
	    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
	        errors.add(error.getDefaultMessage());
	    }
	    
	    
	    ErrorResponse errorResponse = ErrorResponse.builder().errors(errors).message("Request Validation Failed").build();

	    return new ResponseEntity<Object>(errorResponse, HttpStatus.BAD_REQUEST);
	    
	}
	
	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<?> handleServiceException(WebRequest request, ServiceException ex) {
        
		log.error("service layer exception occured {}", ex.getMessage()); 
        HttpStatus status = HttpStatus.BAD_REQUEST;
        
        ErrorResponse errorResponse = ex.getErrorResponse();
        
        return new ResponseEntity<>(errorResponse, status);
    }
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<?> handleDataIntegrityException(WebRequest request, DataIntegrityViolationException ex) {
		
		log.error("service layer exception occured {}", ex.getMessage()); 
        HttpStatus status = HttpStatus.BAD_REQUEST;
        
        List<String> errors = new ArrayList<>();
        errors.add("restaurantName already exist");
        ErrorResponse errorResponse = ErrorResponse.builder()
        										   .errors(errors)
        										   .message("Request Validation Failed")
        										   .build();
        
        return new ResponseEntity<>(errorResponse, status);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleAllException(WebRequest request, Exception ex) {
        
		log.error("Unexpected exception raised {}", ex.getMessage()); 
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        
        ErrorResponse errorMessage = ErrorResponse.builder()
        		                                .message(ex.getMessage())
        		                                .build();
        
        return new ResponseEntity<>(errorMessage, status);
    }
}
