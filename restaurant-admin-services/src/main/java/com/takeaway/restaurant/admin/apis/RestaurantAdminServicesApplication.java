package com.takeaway.restaurant.admin.apis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantAdminServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantAdminServicesApplication.class, args);
	}

}
