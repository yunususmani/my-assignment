# Restaurant OPEN/CLOSED service



## Introduction
This services is implemented based on even driven architecture and fully containarized using docker.

## Components
# Restaurant Admin UI
This is just an admin user interface to get, add, update and delete restaurants.

# Restaurant Admin Services
Rest APIs exposed to support admin user interface. This service also emmit event to Rabbit MQ.

# Admin DB
H2 database to store restaurants details.

# Rabbit MQ
Messaging service, we are using using rabbit-mq docker image to make our components event driven.

# Rabbit MQ Listener
Worker/Listener listening to Rabbit MQ polling events and synchronizing Admin DB with Client DB.

# Client DB
H2 databse to store restaurants details to be used by Restaurant Client App.

# Restaurant Client UI
User interface for Restaurant Clients which will pull data from Client DB and render details on UI.


# Docker 
Each component is a docker container which build the image at runtime.

# Docker Compose
Manages multiple docker containers w.r.t. build, start, stop containers.


# Run Project
docker-compose up -d
