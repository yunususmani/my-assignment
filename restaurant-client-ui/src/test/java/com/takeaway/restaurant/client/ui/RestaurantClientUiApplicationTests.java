package com.takeaway.restaurant.client.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.ui.Model;

import com.takeaway.restaurant.client.ui.controller.RestaurantController;
import com.takeaway.restaurant.client.ui.entity.Restaurant;
import com.takeaway.restaurant.client.ui.model.Status;
import com.takeaway.restaurant.client.ui.repository.RestaurantRepository;
import com.takeaway.restaurant.client.ui.service.RestaurantService;

@ActiveProfiles("junittests")
@SpringBootTest
class RestaurantClientUiApplicationTests {

	@Autowired
	private RestaurantController controller;
	
	@Autowired
	private RestaurantService service;
	
	@MockBean
	private RestaurantRepository repository;
	
	@MockBean
	private Model model;
	
	@Test
	void contextLoads() {
	}

	@Test
	public void testGetRestaurants() {
		
		service.setRepository(repository);
		controller.setService(service);
		
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(new Restaurant(1, "delicious food services", Status.OPEN));
		restaurants.add(new Restaurant(2, "food services", Status.OPEN));
		
		Mockito.when(repository.findAll()).thenReturn(restaurants);
		
		String page = controller.getAllRestaurants(model);
		
		assertEquals(page, "restaurant");
	}
}
