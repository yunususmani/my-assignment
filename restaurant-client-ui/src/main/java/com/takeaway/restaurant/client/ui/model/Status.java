package com.takeaway.restaurant.client.ui.model;

public enum Status {

	OPEN, CLOSED
}
