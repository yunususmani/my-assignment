package com.takeaway.restaurant.client.ui.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

/**
 * Error controller
 * @author Mohdyunus.Usmani
 *
 */
@Slf4j
@Controller
public class MyErrorController implements ErrorController  {

    @RequestMapping("/error")
    public String handleError() {
        log.error("something is wrong, redirecting to error page");
        return "error";
    }
}