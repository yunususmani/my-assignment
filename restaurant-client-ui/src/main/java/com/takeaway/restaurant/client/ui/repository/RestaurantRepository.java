package com.takeaway.restaurant.client.ui.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.takeaway.restaurant.client.ui.entity.Restaurant;

@Transactional
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer>{

}
