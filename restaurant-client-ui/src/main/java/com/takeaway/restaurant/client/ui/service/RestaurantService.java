package com.takeaway.restaurant.client.ui.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.takeaway.restaurant.client.ui.entity.Restaurant;
import com.takeaway.restaurant.client.ui.model.RestaurantResponse;
import com.takeaway.restaurant.client.ui.repository.RestaurantRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Service layer for restaurant frontend service.
 * @author Mohdyunus.Usmani
 *
 */
@Slf4j
@Service
public class RestaurantService {

	@Autowired
	private RestaurantRepository repository;
	
	/**
	 * returns all restaurants
	 * @return
	 */
	public List<RestaurantResponse> getAllRestaurants(){
		
		List<Restaurant> restaurants = repository.findAll();
		
		log.info("all restaurants retrieved from client database");
		
		//sorting in decresing order based on restaurant ids, so that recently added should be displayed on top
		Collections.sort(restaurants, new Comparator<Restaurant>() {

			@Override
			public int compare(Restaurant o1, Restaurant o2) {
				return o2.getId().compareTo(o1.getId());
			}
		});
		
		List<RestaurantResponse> response = new ArrayList<>();
		restaurants.forEach(restaurant ->{
			RestaurantResponse restaurant2 = com.takeaway.restaurant.client.ui.model.RestaurantResponse.builder()
															  .id(restaurant.getId())
															  .restaurantName(restaurant.getName())
															  .status(restaurant.getStatus().toString())
															  .build();
			
			response.add(restaurant2);
		});
		
		return response;
	}

	public RestaurantRepository getRepository() {
		return repository;
	}

	public void setRepository(RestaurantRepository repository) {
		this.repository = repository;
	}
	
}
