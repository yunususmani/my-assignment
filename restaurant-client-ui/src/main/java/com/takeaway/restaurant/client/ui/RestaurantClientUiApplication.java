package com.takeaway.restaurant.client.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantClientUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantClientUiApplication.class, args);
	}

}
