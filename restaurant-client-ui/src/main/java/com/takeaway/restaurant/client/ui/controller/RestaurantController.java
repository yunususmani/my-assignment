package com.takeaway.restaurant.client.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.takeaway.restaurant.client.ui.model.RestaurantResponse;
import com.takeaway.restaurant.client.ui.service.RestaurantService;

import lombok.extern.slf4j.Slf4j;

/**
 * Restaurant frontend controllers
 * @author Mohdyunus.Usmani
 *
 */
@Slf4j
@Controller
public class RestaurantController {
	
	@Autowired
	private RestaurantService service;
	
	/**
	 * returns all restaurants from client database.
	 * @param model
	 * @return
	 */
	@GetMapping
	public String getAllRestaurants(Model model){
		
		log.info("loading all restaurants from client database");
		List<RestaurantResponse> restaurants = service.getAllRestaurants();
		model.addAttribute("restaurant", new RestaurantResponse());
		model.addAttribute("restaurants", restaurants);
		
		return "restaurant";
	}

	public RestaurantService getService() {
		return service;
	}

	public void setService(RestaurantService service) {
		this.service = service;
	}

	
}
