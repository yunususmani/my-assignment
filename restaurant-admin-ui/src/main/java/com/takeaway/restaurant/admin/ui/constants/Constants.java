package com.takeaway.restaurant.admin.ui.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {

	private static String adminServicesURL;

	public static String getAdminServicesURL() {
		return adminServicesURL;
	}

	@Value("${admin.services.url}")
	public void setAdminServicesURL(String adminServicesURL) {
		Constants.adminServicesURL = adminServicesURL;
	}
	
}
