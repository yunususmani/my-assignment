package com.takeaway.restaurant.admin.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantAdminUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantAdminUiApplication.class, args);
	}

}
