package com.takeaway.restaurant.admin.ui.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.takeaway.restaurant.admin.ui.constants.Constants;
import com.takeaway.restaurant.admin.ui.model.ErrorResponse;
import com.takeaway.restaurant.admin.ui.model.Restaurant;

import lombok.extern.slf4j.Slf4j;

/**
 * Restaurant controllers
 * @author Mohdyunus.Usmani
 *
 */
@Slf4j
@Controller
public class RestaurantController {

	ObjectMapper objectMapper = new ObjectMapper();
	
	private RestTemplate restTemplate = new RestTemplate(); 
	
	@GetMapping
	public String getAllRestaurants(Model model){
		
		log.info("get all restaurants request received");
		List<Restaurant> restaurants = getRestaurants();
		model.addAttribute("restaurant", new Restaurant());
		model.addAttribute("restaurants", restaurants);
		model.addAttribute("errorResponse", new ErrorResponse());
		model.addAttribute("action", "retrieve");
		
		return "restaurant";
	}
	
	@PostMapping("/addRestaurant")
	public String addRestaurant(Model model, @ModelAttribute("restaurant") Restaurant restaurant) throws JsonMappingException, JsonProcessingException {
		
		log.info("add restaurant request received");
		
		try {
			restTemplate.postForObject(Constants.getAdminServicesURL(), restaurant, ResponseEntity.class);
		} catch(HttpStatusCodeException e) {
			
			log.error("error response received from backend api");
	        ErrorResponse errorResponse = objectMapper.readValue(e.getResponseBodyAsString(), ErrorResponse.class);
			
			model.addAttribute("errorResponse", errorResponse);
			model.addAttribute("restaurant", restaurant);
			model.addAttribute("restaurants", getRestaurants());
			model.addAttribute("action", "add");
			return "restaurant";
	    }
		
		model.addAttribute("restaurant", new Restaurant());
		model.addAttribute("restaurants", getRestaurants());
		model.addAttribute("errorResponse", new ErrorResponse());
		model.addAttribute("action", "add");
		return "restaurant";
	}
	
	@GetMapping("/editRestaurant")
	public String editRestaurant(Model model, @RequestParam("id") Integer id) {
		
		log.info("edir restaurant request received");
		Restaurant restaurant = restTemplate.getForObject(Constants.getAdminServicesURL()+"/"+id, Restaurant.class);
		model.addAttribute("restaurant", restaurant);
		model.addAttribute("restaurants", getRestaurants());
		model.addAttribute("errorResponse", new ErrorResponse());
		model.addAttribute("action", "update");
		
		return "restaurant";
	}
	
	@PostMapping("/updateRestaurant")
	public String updateRestaurant(Model model, @ModelAttribute("restaurant") Restaurant restaurant) throws JsonMappingException, JsonProcessingException {
		
		log.info("update restaurant request received");
		try {
			HttpEntity<Restaurant> requestEntity = new HttpEntity<Restaurant>(restaurant);
			restTemplate.exchange(Constants.getAdminServicesURL(), HttpMethod.PUT, requestEntity, ResponseEntity.class);
		} catch(HttpStatusCodeException e) {
			
			log.error("error response received from backend api");
	        ErrorResponse errorResponse = objectMapper.readValue(e.getResponseBodyAsString(), ErrorResponse.class);
			
			model.addAttribute("errorResponse", errorResponse);
			model.addAttribute("restaurant", restaurant);
			model.addAttribute("restaurants", getRestaurants());
			model.addAttribute("action", "update");
			return "restaurant";
	    }
		
		model.addAttribute("restaurant", new Restaurant());
		model.addAttribute("restaurants", getRestaurants());
		model.addAttribute("errorResponse", new ErrorResponse());
		model.addAttribute("action", "add");
		
		return "restaurant";
	}
	
	@GetMapping("/deleteRestaurant")
	public String deleteRestaurant(Model model, @RequestParam("id") Integer id) {

		log.info("delete restaurant request received");
		restTemplate.delete(Constants.getAdminServicesURL()+"/"+id);
		List<Restaurant> restaurants = getRestaurants();
		model.addAttribute("restaurant", new Restaurant());
		model.addAttribute("restaurants", restaurants);
		model.addAttribute("errorResponse", new ErrorResponse());
		model.addAttribute("action", "retrieve");
		
		return "restaurant";
	}
	
	public List<Restaurant> getRestaurants(){
		List apiResponse = restTemplate.getForObject(Constants.getAdminServicesURL(), List.class);
		
		List<Restaurant> restaurants = objectMapper.convertValue(
																	apiResponse, 
																	new TypeReference<List<Restaurant>>(){}
																);
		
		Collections.sort(restaurants, new Comparator<Restaurant>() {

			@Override
			public int compare(Restaurant o1, Restaurant o2) {
				// TODO Auto-generated method stub
				return o2.getId().compareTo(o1.getId());
			}
		});
		
		return restaurants;
		
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
}
