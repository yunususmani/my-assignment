package com.takeaway.restaurant.admin.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.takeaway.restaurant.admin.ui.constants.Constants;
import com.takeaway.restaurant.admin.ui.controller.RestaurantController;
import com.takeaway.restaurant.admin.ui.model.Restaurant;

@SpringBootTest
class RestaurantAdminUiApplicationTests {

	@Autowired
	private RestaurantController controller;

	@MockBean
	private Model model;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Test
	void contextLoads() {
	}

	/**
	 * Test get all restaurants
	 */
	@Test
	public void test1() {
		controller.setRestTemplate(restTemplate);
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(new Restaurant(1, "delicious food services", "open"));
		
		Mockito.when(restTemplate.getForObject(Constants.getAdminServicesURL(), List.class)).thenReturn(restaurants);
		String page = controller.getAllRestaurants(model);
		assertEquals(page, "restaurant");
	}
	
	/**
	 * Test add restaurant 
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
	 */
	@Test
	public void test2() throws JsonMappingException, JsonProcessingException {
		controller.setRestTemplate(restTemplate);
		
		Restaurant restaurant = new Restaurant(1, "delicious food services", "open");
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(restaurant);
		Mockito.when(restTemplate.postForObject(Constants.getAdminServicesURL(), restaurant, ResponseEntity.class)).thenReturn(null);
		Mockito.when(restTemplate.getForObject(Constants.getAdminServicesURL(), List.class)).thenReturn(restaurants);

		String page = controller.addRestaurant(model, restaurant);
		assertEquals(page, "restaurant");
	}
	
	/**
	 * Test edit restaurant  
	 */
	@Test
	public void test3() {
		controller.setRestTemplate(restTemplate);
		
		Restaurant restaurant = new Restaurant(1, "delicious food services", "open");
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(restaurant);
		Mockito.when(restTemplate.getForObject(Constants.getAdminServicesURL(), List.class)).thenReturn(restaurants);
		
		Mockito.when(restTemplate.getForObject(Constants.getAdminServicesURL()+"/1", Restaurant.class)).thenReturn(restaurant);

		String page = controller.editRestaurant(model, 1);
		assertEquals(page, "restaurant");
	}
	
	/**
	 * Test update restaurant 
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
	 */
	@Test
	public void test4() throws JsonMappingException, JsonProcessingException {
		controller.setRestTemplate(restTemplate);
		
		Restaurant restaurant = new Restaurant(1, "delicious food services", "open");
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(restaurant);
		Mockito.when(restTemplate.exchange(Mockito.eq(Constants.getAdminServicesURL()), Mockito.eq(HttpMethod.PUT), Mockito.<HttpEntity<Restaurant>> any(), Mockito.eq(ResponseEntity.class))).thenReturn(null);
		Mockito.when(restTemplate.getForObject(Constants.getAdminServicesURL(), List.class)).thenReturn(restaurants);

		String page = controller.updateRestaurant(model, restaurant);
		assertEquals(page, "restaurant");
	}
	
	/**
	 * Test delete restaurant  
	 */
	@Test
	public void test5() {
		controller.setRestTemplate(restTemplate);
		
		Restaurant restaurant = new Restaurant(1, "delicious food services", "open");
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(restaurant);
		Mockito.when(restTemplate.getForObject(Constants.getAdminServicesURL(), List.class)).thenReturn(restaurants);
		
		Mockito.doNothing().when(restTemplate).delete(Constants.getAdminServicesURL()+"/1");

		String page = controller.deleteRestaurant(model, 1);
		assertEquals(page, "restaurant");
	}
}
