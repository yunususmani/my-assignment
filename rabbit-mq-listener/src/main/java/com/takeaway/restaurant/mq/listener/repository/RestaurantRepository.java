package com.takeaway.restaurant.mq.listener.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.takeaway.restaurant.mq.listener.entity.Restaurant;

public interface RestaurantRepository extends JpaRepository<Restaurant, Integer>{

	
}
