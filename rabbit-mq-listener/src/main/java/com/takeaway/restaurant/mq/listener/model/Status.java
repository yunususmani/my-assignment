package com.takeaway.restaurant.mq.listener.model;

public enum Status {

	OPEN, CLOSED
}
