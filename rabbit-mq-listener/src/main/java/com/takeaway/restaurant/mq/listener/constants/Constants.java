package com.takeaway.restaurant.mq.listener.constants;

public class Constants {

	public static final String CREATED = "CREATED";
	public static final String UPDATED = "UPDATED";
	public static final String DELETED = "DELETED";
	
}
