package com.takeaway.restaurant.mq.listener.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.takeaway.restaurant.mq.listener.config.MQConfig;
import com.takeaway.restaurant.mq.listener.constants.Constants;
import com.takeaway.restaurant.mq.listener.entity.Restaurant;
import com.takeaway.restaurant.mq.listener.model.Message;
import com.takeaway.restaurant.mq.listener.model.Status;
import com.takeaway.restaurant.mq.listener.repository.RestaurantRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Rabbit MQ listener class.
 * @author Mohdyunus.Usmani
 *
 */
@Slf4j
@Component
public class MessageListener {

	@Autowired
	private RestaurantRepository repository;
	
	/**
	 * Keep listen to queue and process message as soon as meesage is available.
	 * @param message
	 */
	@RabbitListener(queues = MQConfig.QUEUE)
	public void listener(Message message) {
		
		log.info("message received : {}", message);
		String eventType = message.getEventType();
		
		switch (eventType) {
		case Constants.DELETED:
			repository.deleteById(message.getId());
			break;
		default:
			Restaurant restaurant = Restaurant.builder().id(message.getId())
								.name(message.getRestaurantName())
								.status(Status.valueOf(message.getStatus()))
								.build();
			repository.save(restaurant);
			break;
		}
		
		log.info("message processed : {}", message);
	}
}
