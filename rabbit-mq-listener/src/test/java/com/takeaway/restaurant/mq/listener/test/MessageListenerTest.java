package com.takeaway.restaurant.mq.listener.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.takeaway.restaurant.mq.listener.entity.Restaurant;
import com.takeaway.restaurant.mq.listener.model.Message;
import com.takeaway.restaurant.mq.listener.model.Status;
import com.takeaway.restaurant.mq.listener.repository.RestaurantRepository;
import com.takeaway.restaurant.mq.listener.service.MessageListener;

@ActiveProfiles("junittests")
@SpringBootTest
public class MessageListenerTest {

	@Autowired
	private MessageListener listener;
	
	@Autowired
	private RestaurantRepository repository;
		
	@Test
	public void contextLoads() throws Exception {
		assertThat(listener).isNotNull();
	}
	
	@Test
	public void testCreateEvent() {
		
		Integer id = 123;
		String restaurantName = "delicios food service";
		String status = "OPEN";
		String eventType = "CREATED";
		Message message = Message.builder().messageId(UUID.randomUUID().toString())
						 .id(id)
						 .restaurantName(restaurantName)
						 .status(status)
						 .eventType(eventType)
						 .build();
		
		listener.listener(message);
		
		Optional<Restaurant> optional = repository.findById(id);
		
		assertTrue(optional.isPresent());
		assertEquals(optional.get().getId(), id);
		assertEquals(optional.get().getName(), restaurantName);
		assertEquals(optional.get().getStatus(), Status.valueOf(status));
	}
	
	@Test
	public void testUpdatedEvent() {
		
		Integer id = 123;
		String restaurantName = "delicios food services";
		String status = "CLOSED";
		String eventType = "UPDATED";
		Message message = Message.builder().messageId(UUID.randomUUID().toString())
						 .id(id)
						 .restaurantName(restaurantName)
						 .status(status)
						 .eventType(eventType)
						 .build();
		
		listener.listener(message);
		
		Optional<Restaurant> optional = repository.findById(id);
		
		assertTrue(optional.isPresent());
		assertEquals(optional.get().getId(), id);
		assertEquals(optional.get().getName(), restaurantName);
		assertEquals(optional.get().getStatus(), Status.valueOf(status));
	}
	
	@Test
	public void testDeletedEvent() {
		
		Integer id = 123;
		
		String eventType = "DELETED";
		Message message = Message.builder().messageId(UUID.randomUUID().toString())
						 .id(id)
						 .eventType(eventType)
						 .build();
		
		listener.listener(message);
		
		Optional<Restaurant> optional = repository.findById(id);
		
		assertFalse(optional.isPresent());
	}
	
}
